# Chrome Policies
Just some "dotfiles".
## Install
```bash
git clone https://gitlab.com/sharmaganesha2/cpolicies.git /etc/opt/chrome/policies/managed/
chmod -w /etc/opt/chrome/policies/managed/ --recursive
killall google-chrome-stable
killall chrome
killall google-chrome-unstable
killall google-chrome
google-chrome &
```
## Update
```bash
cd /etc/opt/chrome/policies/managed
git pull
cd -
```
## Remove
```bash
rm -frv /etc/opt/chrome/policies/managed
```
